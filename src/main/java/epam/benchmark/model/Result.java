package epam.benchmark.model;

public abstract class Result {

	private String nameCollection;
	private long memoryUsed;
	private long timeAddObjects;
	private CollectionType collectionType;

	public String getNameCollection() {
		return nameCollection;
	}

	public void setNameCollection(String nameCollection) {
		this.nameCollection = nameCollection;
	}

	public Long getMemoryUsed() {
		return memoryUsed;
	}

	public void setMemoryUsed(Long memoryUsed) {
		this.memoryUsed = memoryUsed;
	}

	public Long getTimeAddObjects() {
		return timeAddObjects;
	}

	public void setTimeAddObjects(Long timeAddObjects) {
		this.timeAddObjects = timeAddObjects;
	}

	public CollectionType getCollectionType() {
		return collectionType;
	}

	public void setCollectionType(CollectionType collectionType) {
		this.collectionType = collectionType;
	}

	
	
	

}
