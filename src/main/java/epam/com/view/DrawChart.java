package epam.com.view;

import java.lang.reflect.Field;

import java.util.LinkedList;
import java.util.List;

import epam.benchmark.ContainerCollection;
import epam.benchmark.Executor;
import epam.benchmark.model.CollectionType;
import epam.benchmark.model.Result;

import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.scene.Group;

public class DrawChart extends Application {

	private static final String STYLE_CODE = "-fx-background-color: #336699;";
	private static final String OPERATION = "Operation";
	private static final String LABEL_TIME_MEMORY = "time/memory";
	private static final String BUTTON_REDUCE_ZOOM = "zoom -";
	private static final String BUTTON_ZOOM = "zoom +";
	private static final String TITLE = "Chart Collection";
	private List result;
	private DoubleProperty zoomProperty = new SimpleDoubleProperty(200);

	@Override
	public void start(Stage primaryStage) throws Exception {

		VBox content = new VBox();

		ScrollPane scroller = new ScrollPane(content);
		scroller.setFitToWidth(true);
		primaryStage.setTitle(TITLE);
		result = ContainerCollection.getResult();

		Button buttonOffZoom = new Button();
		buttonOffZoom.setText(BUTTON_ZOOM);

		Button buttonOnZoom = new Button();
		buttonOnZoom.setText(BUTTON_REDUCE_ZOOM);

		drawChart(content);

		buttonOffZoom.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				zoomProperty.set(zoomProperty.get() * 1.1);
			}
		});

		buttonOnZoom.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				zoomProperty.set(zoomProperty.get() / 1.1);
			}
		});

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(15, 12, 15, 12));
		hbox.setSpacing(10);
		hbox.setStyle(STYLE_CODE);
		hbox.getChildren().addAll(buttonOffZoom, buttonOnZoom);

		Scene scene = new Scene(new BorderPane(scroller, hbox, null, null, null), 1200, 900);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void drawChart(VBox content) {
		for (CollectionType collectionType : CollectionType.values()) {
			NumberAxis xAxis = new NumberAxis();
			CategoryAxis yAxis = new CategoryAxis();
			BarChart<Number, String> bc = new BarChart<>(xAxis, yAxis);
			bc.setTitle(collectionType.getName());
			xAxis.setLabel(LABEL_TIME_MEMORY);
			xAxis.setTickLabelRotation(90);
			yAxis.setLabel(OPERATION);

			bc.getData().addAll(convertToSeries(result, collectionType));

			bc.setPrefSize(800, 800);
			VBox.setVgrow(bc, Priority.ALWAYS);

			zoomProperty.addListener(new InvalidationListener() {
				@Override
				public void invalidated(Observable arg0) {
					bc.setPrefWidth(zoomProperty.get() * 4);
					bc.setPrefHeight(zoomProperty.get() * 4);
				}
			});

			content.getChildren().add(bc);

		}
	}

	private List<Series<Number, String>> convertToSeries(List<Result> resultObjects, CollectionType collectionType) {
		List<Series<Number, String>> listSeries = new LinkedList<>();
		for (Result resultObject : resultObjects) {
			if (resultObject.getCollectionType().equals(collectionType)) {
				listSeries.add(getSeries(resultObject));
			}
		}
		return listSeries;
	}

	private XYChart.Series getSeries(Result resultObject) {
		XYChart.Series series = new XYChart.Series();

		series.setName(resultObject.getNameCollection());
		Field[] fields = resultObject.getClass().getDeclaredFields();
		fields = Executor.joinArrays(fields, resultObject.getClass().getSuperclass().getDeclaredFields());
		for (Field field : fields) {
			if (field.getType().isAssignableFrom(long.class)) {
				field.setAccessible(true);
				try {
					XYChart.Data data = new XYChart.Data(field.get(resultObject), field.getName());
					series.getData().add(data);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		return series;
	}

	private class ZoomingPane extends Pane {
		Node content;
		private DoubleProperty zoomFactor = new SimpleDoubleProperty(1);

		private ZoomingPane(Node content) {
			this.content = content;
			getChildren().add(content);
			Scale scale = new Scale(1, 1);
			content.getTransforms().add(scale);

			zoomFactor.addListener(new ChangeListener<Number>() {
				@Override
				public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					scale.setX(newValue.doubleValue());
					scale.setY(newValue.doubleValue());
					requestLayout();
				}
			});
		}

		@Override
		protected void layoutChildren() {
			Pos pos = Pos.TOP_LEFT;
			double width = getWidth();
			double height = getHeight();
			double top = getInsets().getTop();
			double right = getInsets().getRight();
			double left = getInsets().getLeft();
			double bottom = getInsets().getBottom();
			double contentWidth = (width - left - right) / zoomFactor.get();
			double contentHeight = (height - top - bottom) / zoomFactor.get();
			layoutInArea(content, left, top, contentWidth, contentHeight, 0, null, pos.getHpos(), pos.getVpos());
		}

		public final Double getZoomFactor() {
			return zoomFactor.get();
		}

		public final void setZoomFactor(Double zoomFactor) {
			this.zoomFactor.set(zoomFactor);
		}

		public final DoubleProperty zoomFactorProperty() {
			return zoomFactor;
		}
	}

}
