package epam.benchmark;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import java.util.WeakHashMap;

import epam.benchmark.collection.CollectionMeasurementList;
import epam.benchmark.collection.CollectionMeasurementMap;
import epam.benchmark.collection.CollectionMeasurementQueue;
import epam.benchmark.collection.CollectionMeasurementSet;
import epam.benchmark.model.Result;
import epam.benchmark.model.ResultList;
import epam.benchmark.model.ResultMap;
import epam.benchmark.model.ResultQueue;
import epam.benchmark.model.ResultSet;


import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.PriorityBlockingQueue;

import javax.management.relation.RoleList;
import javax.management.relation.RoleUnresolvedList;
import org.apache.commons.collections4.list.CursorableLinkedList;
import org.apache.commons.collections4.list.GrowthList;
import org.apache.commons.collections4.list.NodeCachingLinkedList;
import org.apache.commons.collections4.list.TreeList;
import org.apache.commons.collections4.map.Flat3Map;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.collections4.map.LRUMap;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.collections4.map.ListOrderedMap;
import org.apache.commons.collections4.queue.CircularFifoQueue;

import org.apache.commons.collections4.set.ListOrderedSet;

public class ContainerCollection {
	
	private static List<Result> resultList = new ArrayList<>();
	private Executor<Result> executor;
	
	public ContainerCollection() {
		executor = new Executor<>();
		
	}
	
	public void fill(){
		
		resultList.add(executor.run(new CollectionMeasurementSet<HashSet>(new HashSet()), new ResultSet()));
		resultList.add(executor.run(new CollectionMeasurementSet<TreeSet>(new TreeSet()), new ResultSet()));
		resultList.add(executor.run(new CollectionMeasurementSet<LinkedHashSet>(new LinkedHashSet()), new ResultSet()));
		resultList.add(executor.run(new CollectionMeasurementSet<CopyOnWriteArraySet>(new CopyOnWriteArraySet()), new ResultSet()));
		resultList.add(executor.run(new CollectionMeasurementSet<ConcurrentSkipListSet>(new ConcurrentSkipListSet()), new ResultSet()));
		resultList.add(executor.run(new CollectionMeasurementSet<ListOrderedSet>(new ListOrderedSet()), new ResultSet()));
		


		
		resultList.add(executor.run(new CollectionMeasurementList<ArrayList>(new ArrayList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<LinkedList>(new LinkedList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<CopyOnWriteArrayList>(new CopyOnWriteArrayList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<RoleList>(new  RoleList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<RoleUnresolvedList>(new RoleUnresolvedList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<Stack>(new Stack()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<Vector>(new Vector()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<CursorableLinkedList>(new CursorableLinkedList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<GrowthList>(new GrowthList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<NodeCachingLinkedList>(new NodeCachingLinkedList()), new ResultList()));
		resultList.add(executor.run(new CollectionMeasurementList<TreeList>(new TreeList()), new ResultList()));
		
		
		
		resultList.add(executor.run(new CollectionMeasurementQueue<PriorityQueue>(new PriorityQueue()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<ArrayDeque>(new ArrayDeque()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<ConcurrentLinkedDeque>(new ConcurrentLinkedDeque()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<PriorityQueue>(new PriorityQueue()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<LinkedBlockingDeque>(new LinkedBlockingDeque()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<LinkedBlockingQueue>(new LinkedBlockingQueue()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<LinkedTransferQueue>(new LinkedTransferQueue()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<PriorityBlockingQueue>(new PriorityBlockingQueue()), new ResultQueue()));
		resultList.add(executor.run(new CollectionMeasurementQueue<CircularFifoQueue>(new CircularFifoQueue()), new ResultQueue()));
		
		
		resultList.add(executor.run(new CollectionMeasurementMap<ConcurrentHashMap>(new ConcurrentHashMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<ConcurrentSkipListMap>(new ConcurrentSkipListMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<HashMap>(new HashMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<Hashtable>(new Hashtable()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<IdentityHashMap>(new IdentityHashMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<LinkedHashMap>(new LinkedHashMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<TreeMap>(new TreeMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<WeakHashMap>(new WeakHashMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<Flat3Map>(new Flat3Map()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<HashedMap>(new HashedMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<LinkedMap>(new LinkedMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<ListOrderedMap>(new ListOrderedMap()), new ResultMap()));
		resultList.add(executor.run(new CollectionMeasurementMap<LRUMap>(new LRUMap()), new ResultMap()));
		
	}
	
	public static List<Result> getResult(){
		return resultList;
	}

}
