package epam.benchmark.collection;

import java.util.Set;

import epam.benchmark.model.CollectionType;
import epam.benchmark.model.ResultSet;

public class CollectionMeasurementSet<E extends Set> extends CollectionMeasurement<E> {

	private static final int END_ENTRY_INDEX = 1000;
	private static final int START_ENTRY_INDEX = 0;
	private static final String INDEX = "index";
	private static final String CONTAINS_VALUE = "contains";
	private static final String MIDLE_VALUE = "index500";
	private static final String REMOVE_VALUE = "index1";
	private ResultSet result;

	public CollectionMeasurementSet(E collection) {
		super(collection);

	}

	private void addObjects() {
		clearCollection();

		long startTime = System.nanoTime();
		fillColection();
		long endTime = System.nanoTime();

		result.setTimeAddObjects(getTime(startTime, endTime));
	}

	private void usedMemory() {
		clearCollection();

		long startMemory = getMemory();
		fillColection();
		long endMemory = getMemory();

		result.setMemoryUsed(getMemoryUsed(startMemory, endMemory));
	}

	private void remove() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(REMOVE_VALUE);
		long endTime = System.nanoTime();

		result.setTimeRemove(getTime(startTime, endTime));

	}

	private void removeTail() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(REMOVE_VALUE);
		long endTime = System.nanoTime();

		result.setTimeRemoveFromTail(getTime(startTime, endTime));

	}

	private void removeFromMidle() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(MIDLE_VALUE);
		long endTime = System.nanoTime();

		result.setTimeRemoveFromMidle(getTime(startTime, endTime));
	}

	private void contains() {
		clearCollection();
		fillColection();

		collection.add(CONTAINS_VALUE);
		long startTime = System.nanoTime();
		collection.contains(CONTAINS_VALUE);
		long endTime = System.nanoTime();

		result.setTimeContainsElement(getTime(startTime, endTime));
	}

	@Override
	protected void clearCollection() {
		collection.clear();
	}

	@Override
	protected void fillColection() {
		for (int i = START_ENTRY_INDEX; i < END_ENTRY_INDEX; i++) {
			collection.add(new String(INDEX + i));
		}
	}

	public ResultSet getResult() {
		return result;
	}

	@Override
	public ResultSet call() throws Exception {
		result = new ResultSet();
		result.setNameCollection(collection.getClass().getName());
		result.setCollectionType(CollectionType.SET);
		addObjects();
		usedMemory();
		remove();
		removeTail();
		removeFromMidle();
		contains();
		return result;
	}

}
