package epam.benchmark.model;

public class ResultMap extends Result{
	
	private long timeRemove;
	private long timeContainsKey;
	private long timeContainsVelue;
	private long timeReplace;
	private long timeGetElement;

	public Long getTimeRemove() {
		return timeRemove;
	}

	public void setTimeRemove(Long timeRemove) {
		this.timeRemove = timeRemove;
	}

	public Long getTimeContainsKey() {
		return timeContainsKey;
	}

	public void setTimeContainsKey(Long timeContainsKey) {
		this.timeContainsKey = timeContainsKey;
	}

	public Long getTimeContainsVelue() {
		return timeContainsVelue;
	}

	public void setTimeContainsVelue(Long timeContainsVelue) {
		this.timeContainsVelue = timeContainsVelue;
	}

	public Long getTimeReplace() {
		return timeReplace;
	}

	public void setTimeReplace(Long timeReplace) {
		this.timeReplace = timeReplace;
	}

	public Long getTimeGetElement() {
		return timeGetElement;
	}

	public void setTimeGetElement(Long timeGetElement) {
		this.timeGetElement = timeGetElement;
	}

	@Override
	public String toString() {
		return "ResultMap [timeRemove=" + timeRemove + ", timeContainsKey="
				+ timeContainsKey + ", timeContainsVelue=" + timeContainsVelue
				+ ", timeReplace=" + timeReplace + ", timeGetElement="
				+ timeGetElement + ", getNameCollection()="
				+ getNameCollection() + ", getMemoryUsed()=" + getMemoryUsed()
				+ ", getTimeAddObjects()=" + getTimeAddObjects() + "]";
	}
	
	

}
