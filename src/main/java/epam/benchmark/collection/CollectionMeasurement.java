package epam.benchmark.collection;

import java.util.concurrent.Callable;

public abstract class CollectionMeasurement<E> implements Callable {

	protected E collection;
	private Runtime runtime;

	public CollectionMeasurement(E collection) {
		this.collection = collection;
		runtime = Runtime.getRuntime();
	}

	protected long getTime(long start, long end) {
		return end - start;
	}

	protected long getMemoryUsed(long startMemory, long endMemory) {
		return endMemory - startMemory;
	}

	protected long getMemory() {

		runtime.gc();

		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}

	protected abstract void clearCollection();

	protected abstract void fillColection();
}
