package epam.benchmark.model;

public class ResultSet extends Result {

	private long timeRemove;
	private long timeRemoveFromMidle;
	private long timeRemoveFromTail;
	private long timeContainsElement;

	public Long getTimeRemove() {
		return timeRemove;
	}

	public void setTimeRemove(Long timeRemove) {
		this.timeRemove = timeRemove;
	}

	public Long getTimeRemoveFromTail() {
		return timeRemoveFromTail;
	}

	public void setTimeRemoveFromTail(Long timeRemoveFromTail) {
		this.timeRemoveFromTail = timeRemoveFromTail;
	}

	public Long getTimeRemoveFromMidle() {
		return timeRemoveFromMidle;
	}

	public void setTimeRemoveFromMidle(Long timeRemoveFromMidle) {
		this.timeRemoveFromMidle = timeRemoveFromMidle;
	}


	public Long getTimeContainsElement() {
		return timeContainsElement;
	}

	public void setTimeContainsElement(Long timeContainsElement) {
		this.timeContainsElement = timeContainsElement;
	}

	@Override
	public String toString() {
		return "ResultSet [timeRemove=" + timeRemove + ", timeRemoveFromMidle=" + timeRemoveFromMidle
				+ ", timeRemoveFromTail=" + timeRemoveFromTail + ", timeContainsElement=" + timeContainsElement
				+ ", getNameCollection()=" + getNameCollection() + ", getMemoryUsed()=" + getMemoryUsed()
				+ ", getTimeAddObjects()=" + getTimeAddObjects() + "]";
	}

	

	

	
}
