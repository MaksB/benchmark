package epam.benchmark.model;

public enum CollectionType {
	LIST("List"), SET("Set"), QUEUE("Queue"), MAP("Map");

	private CollectionType(String name) {
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}


}
