package epam.benchmark;

import java.lang.reflect.Field;
import java.util.ArrayList;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import epam.benchmark.collection.CollectionMeasurement;
import epam.benchmark.model.Result;

public class Executor<E extends Result> {

	private List<E> resultList;
	private E result;
	private static final int SRC_POSITION = 0;
	private static final int DEST_POSITION = 0;

	public E run(CollectionMeasurement<?> collectionMeasurement, E result) {
		resultList = new ArrayList<>();
		this.result = result;
		ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
		for (int i = 0; i < 10; i++) {
			try {
				resultList.add((E) executor.submit(collectionMeasurement).get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}

		}
		executor.shutdown();
		return sum();

	}

	private E sum() {
		Field[] fieldResult = initResultFields();

		try {
			for (E resultL : resultList) {

				Field[] fields = getFields(resultL);
				calculateSumForResultFields(fieldResult, resultL, fields);
			}

			calculateArithmeticMeanForResultFields(fieldResult);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return result;
	}

	private Field[] getFields(E resultL) {
		Field[] fields = resultL.getClass().getDeclaredFields();
		fields = joinArrays(fields, resultL.getClass().getSuperclass().getDeclaredFields());
		return fields;
	}

	private Field[] initResultFields() {
		Field[] fieldResult = result.getClass().getDeclaredFields();
		fieldResult = joinArrays(fieldResult, result.getClass().getSuperclass().getDeclaredFields());
		return fieldResult;
	}

	private void calculateSumForResultFields(Field[] fieldResult, E resultL, Field[] fields)
			throws IllegalAccessException {
		for (int i = 0; i < fieldResult.length; i++) {
			enableAccess(fieldResult, i);
			enableAccess(fields, i);

			if (isAssignable(fieldResult, i)) {
				long sum = fieldResult[i].getLong(result) + fields[i].getLong(resultL);
				fieldResult[i].setLong(result, sum);

			} else {
				fieldResult[i].set(result, fields[i].get(resultL));
			}
		}
	}

	private void enableAccess(Field[] fields, int i) {
		fields[i].setAccessible(true);
	}

	private void calculateArithmeticMeanForResultFields(Field[] fieldResult) throws IllegalAccessException {
		for (int i = 0; i < fieldResult.length; i++) {
			if (isAssignable(fieldResult, i)) {
				long val = fieldResult[i].getLong(result) / resultList.size();
				fieldResult[i].setLong(result, val);
			}
		}
	}

	private boolean isAssignable(Field[] fieldResult, int i) {
		return fieldResult[i].getType().isAssignableFrom(long.class);
	}

	public static Field[] joinArrays(Field[] first, Field[] second) {
		Field[] union = new Field[first.length + second.length];
		System.arraycopy(first, SRC_POSITION, union,DEST_POSITION, first.length);
		System.arraycopy(second, SRC_POSITION, union, first.length, second.length);
		return union;
	}
}
