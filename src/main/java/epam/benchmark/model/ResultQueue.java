package epam.benchmark.model;

public class ResultQueue extends Result {

	private long timeRemove;
	private long timeRemoveFromMidle;
	private long timeRemoveFromTail;
	private long timeContainsElement;
	private long timePeek;
	private long timeOffer;
	private long timePoll;

	public Long getTimeRemove() {
		return timeRemove;
	}

	public void setTimeRemove(Long timeRemove) {
		this.timeRemove = timeRemove;
	}

	public Long getTimeRemoveFromTail() {
		return timeRemoveFromTail;
	}

	public void setTimeRemoveFromTail(Long timeRemoveFromTail) {
		this.timeRemoveFromTail = timeRemoveFromTail;
	}

	public Long getTimeRemoveFromMidle() {
		return timeRemoveFromMidle;
	}

	public void setTimeRemoveFromMidle(Long timeRemoveFromMidle) {
		this.timeRemoveFromMidle = timeRemoveFromMidle;
	}


	public Long getTimeContainsElement() {
		return timeContainsElement;
	}

	public void setTimeContainsElement(Long timeContainsElement) {
		this.timeContainsElement = timeContainsElement;
	}

	public Long getTimePeek() {
		return timePeek;
	}

	public void setTimePeek(Long timePeek) {
		this.timePeek = timePeek;
	}

	public Long getTimeOffer() {
		return timeOffer;
	}

	public void setTimeOffer(Long timeOffer) {
		this.timeOffer = timeOffer;
	}

	public Long getTimePoll() {
		return timePoll;
	}

	public void setTimePoll(Long timePoll) {
		this.timePoll = timePoll;
	}

	@Override
	public String toString() {
		return "ResultQueue [timeRemove=" + timeRemove
				+ ", timeRemoveFromMidle=" + timeRemoveFromMidle
				+ ", timeRemoveFromTail=" + timeRemoveFromTail
				+ ", timeContainsElement=" + timeContainsElement
				+ ", timePeek=" + timePeek + ", timeOffer=" + timeOffer
				+ ", timePoll=" + timePoll + ", getNameCollection()="
				+ getNameCollection() + ", getMemoryUsed()=" + getMemoryUsed()
				+ ", getTimeAddObjects()=" + getTimeAddObjects() + "]";
	}

	

}
