package epam.benchmark.model;

public class ResultList extends Result{

	private long timeSetInMidle;
	private long timeAddInTail;
	private long timeRemove;
	private long timeRemoveFromMidle;
	private long timeRemoveFromTail;
	private long timeGetElement;
	private long timeContainsElement;

	public Long getTimeSetInMidle() {
		return timeSetInMidle;
	}

	public void setTimeSetInMidle(Long timeSetInMidle) {
		this.timeSetInMidle = timeSetInMidle;
	}

	public Long getTimeAddInTail() {
		return timeAddInTail;
	}

	public void setTimeAddInTail(Long timeAddInTail) {
		this.timeAddInTail = timeAddInTail;
	}

	public Long getTimeRemove() {
		return timeRemove;
	}

	public void setTimeRemove(Long timeRemove) {
		this.timeRemove = timeRemove;
	}

	public Long getTimeRemoveFromTail() {
		return timeRemoveFromTail;
	}

	public void setTimeRemoveFromTail(Long timeRemoveFromTail) {
		this.timeRemoveFromTail = timeRemoveFromTail;
	}

	public Long getTimeRemoveFromMidle() {
		return timeRemoveFromMidle;
	}

	public void setTimeRemoveFromMidle(Long timeRemoveFromMidle) {
		this.timeRemoveFromMidle = timeRemoveFromMidle;
	}

	public Long getTimeGetElement() {
		return timeGetElement;
	}

	public void setTimeGetElement(Long timeGetElement) {
		this.timeGetElement = timeGetElement;
	}

	public Long getTimeContainsElement() {
		return timeContainsElement;
	}

	public void setTimeContainsElement(Long timeContainsElement) {
		this.timeContainsElement = timeContainsElement;
	}

	@Override
	public String toString() {
		return "ResultList [timeSetInMidle=" + timeSetInMidle
				+ ", timeAddInTail=" + timeAddInTail + ", timeRemove="
				+ timeRemove + ", timeRemoveFromMidle=" + timeRemoveFromMidle
				+ ", timeRemoveFromTail=" + timeRemoveFromTail
				+ ", timeGetElement=" + timeGetElement
				+ ", timeContainsElement=" + timeContainsElement
				+ ", getNameCollection()=" + getNameCollection()
				+ ", getMemoryUsed()=" + getMemoryUsed()
				+ ", getTimeAddObjects()=" + getTimeAddObjects() + "]";
	}






	

}
