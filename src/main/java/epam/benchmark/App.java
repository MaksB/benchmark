package epam.benchmark;

import epam.com.view.DrawChart;

public class App {

	public static void main(String[] args) {

		execut();
	}

	private static void execut() {
		ContainerCollection collection = new ContainerCollection();
		collection.fill();

		new Thread() {
			@Override
			public void run() {
				javafx.application.Application.launch(DrawChart.class);
			}
		}.start();
	}

}
