package epam.benchmark.collection;

import java.util.List;

import epam.benchmark.model.CollectionType;
import epam.benchmark.model.ResultList;

public class CollectionMeasurementList<E extends List> extends
		CollectionMeasurement<E> {

	private static final String INDEX = "index";
	private ResultList result;
	
	public CollectionMeasurementList(E collection) {
		super(collection);
		
	}


	private void usedMemory() {
		clearCollection();

		long startMemory = getMemory();
		fillColection();
		long endMemory = getMemory();

		result.setMemoryUsed(getMemoryUsed(startMemory, endMemory));
	}

	private void addObjects() {
		clearCollection();

		long startTime = System.nanoTime();
		fillColection();
		long endTime = System.nanoTime();

		result.setTimeAddObjects(getTime(startTime, endTime));

	}

	private void addInMidle() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.set(collection.size() / 2, new String(INDEX));
		long endTime = System.nanoTime();

		result.setTimeSetInMidle(getTime(startTime, endTime));
	}

	private void addInTail() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.set(collection.size() - 1, new String(INDEX));
		long endTime = System.nanoTime();

		result.setTimeAddInTail(getTime(startTime, endTime));
	}

	private void remove() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(1);
		long endTime = System.nanoTime();

		result.setTimeRemove(getTime(startTime, endTime));

	}

	private void removeTail() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(collection.size() - 1);
		long endTime = System.nanoTime();

		result.setTimeRemoveFromTail(getTime(startTime, endTime));

	}

	private void removeFromMidle() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(collection.size() / 2);
		long endTime = System.nanoTime();

		result.setTimeRemoveFromMidle(getTime(startTime, endTime));
	}

	private void getElement() {

		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.get(collection.size() / 2);
		long endTime = System.nanoTime();

		result.setTimeGetElement(getTime(startTime, endTime));
	}

	private void contains() {
		clearCollection();
		fillColection();

		collection.add("contains");
		long startTime = System.nanoTime();
		collection.contains("contains");
		long endTime = System.nanoTime();

		result.setTimeContainsElement(getTime(startTime, endTime));
	}
	@Override
	protected void clearCollection() {
		collection.clear();
	}

	@Override
	protected void fillColection() {
		for (int i = 0; i < 1000; i++) {
			collection.add(new String(INDEX));
		}
	}
	
	public ResultList getResult() {
		return result;
	}

	@Override
	public ResultList call() throws Exception {
		result = new ResultList();
		result.setNameCollection(collection.getClass().getName());
		result.setCollectionType(CollectionType.LIST);
		addObjects();
		addInMidle();
		usedMemory();
		addInTail();
		remove();
		removeTail();
		removeFromMidle();
		getElement();
		contains();
		return getResult();
	}
}
