package epam.benchmark.collection;

import java.util.Queue;

import epam.benchmark.model.CollectionType;
import epam.benchmark.model.ResultQueue;

public class CollectionMeasurementQueue<E extends Queue> extends CollectionMeasurement<E> {

	private static final String INDEX_MIDLE = "index500";
	private static final String INDEX_TAIL = "index999";
	private static final String CONTAINS = "contains";
	private static final String INDEX = "index";
	private ResultQueue resultQueue;

	public CollectionMeasurementQueue(E collection) {
		super(collection);

	}

	private void usedMemory() {
		clearCollection();

		long startMemory = getMemory();
		fillColection();
		long endMemory = getMemory();

		resultQueue.setMemoryUsed(getMemoryUsed(startMemory, endMemory));
	}

	private void addObjects() {
		clearCollection();

		long startTime = System.nanoTime();
		fillColection();
		long endTime = System.nanoTime();

		resultQueue.setTimeAddObjects(getTime(startTime, endTime));

	}

	private void remove() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove();
		long endTime = System.nanoTime();

		resultQueue.setTimeRemove(getTime(startTime, endTime));

	}

	private void removeTail() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(INDEX_TAIL);
		long endTime = System.nanoTime();

		resultQueue.setTimeRemoveFromTail(getTime(startTime, endTime));

	}

	private void removeFromMidle() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(INDEX_MIDLE);
		long endTime = System.nanoTime();

		resultQueue.setTimeRemoveFromMidle(getTime(startTime, endTime));
	}

	private void peek() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.peek();
		long endTime = System.nanoTime();

		resultQueue.setTimePeek(getTime(startTime, endTime));
	}

	private void offer() {
		clearCollection();

		long startTime = System.nanoTime();
		for (int i = 0; i < 1000; i++) {
			collection.offer(INDEX);
		}
		long endTime = System.nanoTime();

		resultQueue.setTimeOffer(getTime(startTime, endTime));
	}

	private void poll() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.poll();
		long endTime = System.nanoTime();

		resultQueue.setTimePoll(getTime(startTime, endTime));
	}

	private void contains() {
		clearCollection();
		fillColection();

		collection.add(CONTAINS);
		long startTime = System.nanoTime();
		collection.contains(CONTAINS);
		long endTime = System.nanoTime();

		resultQueue.setTimeContainsElement(getTime(startTime, endTime));
	}

	@Override
	protected void clearCollection() {
		collection.clear();

	}

	@Override
	protected void fillColection() {
		for (int i = 0; i < 1000; i++) {
			collection.add(new String(INDEX + i));
		}

	}

	public ResultQueue getResult() {
		return resultQueue;
	}

	@Override
	public ResultQueue call() throws Exception {
		resultQueue = new ResultQueue();
		resultQueue.setNameCollection(collection.getClass().getName());
		resultQueue.setCollectionType(CollectionType.QUEUE);
		usedMemory();
		addObjects();
		remove();
		removeTail();
		removeFromMidle();
		peek();
		offer();
		poll();
		contains();
		return getResult();
	}

}
