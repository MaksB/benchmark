package epam.benchmark.collection;

import java.util.Map;

import epam.benchmark.model.CollectionType;
import epam.benchmark.model.ResultMap;

public class CollectionMeasurementMap<E extends Map> extends CollectionMeasurement<E> {

	private static final int END_ENTRY_INDEX = 1000;
	private static final int START_ENTRY_INDEX = 0;
	private static final String NEW_ELEMENT = "newElement";
	private static final String VALUE_MIDLE = "index500";
	private static final int KEY = 1;
	private static final int INDEX_MIDLE = 500;
	private static final String INDEX = "index";
	private ResultMap resultMap;

	public CollectionMeasurementMap(E collection) {
		super(collection);

	}

	private void putObjects() {
		clearCollection();

		long startTime = System.nanoTime();
		fillColection();
		long endTime = System.nanoTime();

		resultMap.setTimeAddObjects(getTime(startTime, endTime));
	}

	private void usedMemory() {
		clearCollection();

		long startMemory = getMemory();
		fillColection();
		long endMemory = getMemory();

		resultMap.setMemoryUsed(getMemoryUsed(startMemory, endMemory));
	}

	private void remove() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.remove(INDEX_MIDLE);
		long endTime = System.nanoTime();

		resultMap.setTimeRemove(getTime(startTime, endTime));

	}

	private void containsKey() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.containsKey(KEY);
		long endTime = System.nanoTime();

		resultMap.setTimeContainsKey(getTime(startTime, endTime));
	}

	private void containsValue() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.containsValue(VALUE_MIDLE);
		long endTime = System.nanoTime();

		resultMap.setTimeContainsVelue(getTime(startTime, endTime));
	}

	private void replace() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.replace(INDEX_MIDLE, NEW_ELEMENT);
		long endTime = System.nanoTime();

		resultMap.setTimeReplace(getTime(startTime, endTime));
	}

	private void getElement() {
		clearCollection();
		fillColection();

		long startTime = System.nanoTime();
		collection.get(300);
		long endTime = System.nanoTime();

		resultMap.setTimeGetElement(getTime(startTime, endTime));
	}

	@Override
	protected void clearCollection() {
		collection.clear();
	}

	@Override
	protected void fillColection() {
		for (int i = START_ENTRY_INDEX; i < END_ENTRY_INDEX; i++) {
			collection.put(i, INDEX);
		}

	}

	public ResultMap getResult() {
		return resultMap;
	}

	@Override
	public ResultMap call() throws Exception {
		resultMap = new ResultMap();
		resultMap.setNameCollection(collection.getClass().getName());
		resultMap.setCollectionType(CollectionType.MAP);
		putObjects();
		usedMemory();
		remove();
		containsKey();
		containsValue();
		replace();
		getElement();
		return getResult();
	}

}
